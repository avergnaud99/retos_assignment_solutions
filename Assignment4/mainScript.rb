# Command line for making the blast database of the S.pombe into the folder Database_pep :
# "/usr/local/Cellar/blast/2.12.0/bin/makeblastdb -in ./Database_pep/pep.fa -dbtype 'prot' -out ./Database_pep/pep.fa"

# Command line for making the blast database of the Arabidopsis genes in the folder Database_TAIR :
# "/usr/local/Cellar/blast/2.12.0/bin/makeblastdb -in ./Database_TAIR/TAIR10_cds_20101214_updated.fa -dbtype 'nucl' -out ./Database_TAIR/TAIR10_cds_20101214_updated.fa"

require 'bio'

fasta_sequences = Bio::FlatFile.auto("./Database_TAIR/TAIR10_cds_20101214_updated.fa")
pep_sequences = Bio::FlatFile.auto("./Database_pep/pep.fa")

# BLAST query : Arabidopsis / BLAST target : S.pombe
best_hits = {}
# BLAST query : S.pombe / BLAST target : Arabidopsis
best_hits_pep = {}

# Finding the best hits of BLAST of the Arabidopsis genes over the pep database
fasta_sequences.each do |seq|
    # Necessity of creating a file in order to compute the command from the terminal
    File.open("current_seq.fa", "w"){ |f|
        f.puts seq
    }
    # The blastx program allows us to blast a nucleotide sequence over a protein database
    pre_report = `/usr/local/Cellar/blast/2.12.0/bin/blastx -query ./current_seq.fa -db ./Database_pep/pep.fa -outfmt 6`
    report = Bio::Blast::Report.new(pre_report) # saving the report as a Bio Report Object
    evalue_dict = {}
    report.each do |hit|
        next unless hit.percent_identity > 20 # verifying that the identity percentage is higher than 20%
        evalue_dict[hit.target_id] = hit.evalue # saving the evalue associated with the target
    end
    # Search of the target with the minimal evalue
    if evalue_dict != {}
        top = evalue_dict.values.min
        best_hits[seq.entry_id] = evalue_dict.index(top)
    end
end

# Finding the best hits of BLAST of the S.pombe genes over the TAIR database
pep_sequences.each do |pseq|
    # Necessity of creating a file in order to compute the command from the terminal
    File.open("current_seq_pep.fa", "w"){ |f|
        f.puts pseq
    }
    # The tblastn program allows us to blast a nucleotide sequence over a protein database
    pre_report_pep = `/usr/local/Cellar/blast/2.12.0/bin/tblastn -query ./current_seq_pep.fa -db ./Database_TAIR/TAIR10_cds_20101214_updated.fa -outfmt 6`
    report_pep = Bio::Blast::Report.new(pre_report_pep) # saving the report as a Bio Report Object
    evalue_dict_pep = {}
    report_pep.each do |hit|
        next unless hit.percent_identity > 20 # verifying that the identity percentage is higher than 20%
        evalue_dict_pep[hit.target_id] = hit.evalue # saving the evalue associated with the target
    end
    # Search of the target with the minimal evalue 
    if evalue_dict_pep != {}
        top_pep = evalue_dict_pep.values.min
        best_hits_pep[pseq.entry_id] = evalue_dict_pep.index(top_pep)
    end
end

puts best_hits
puts best_hits_pep

puts "-----FINAL REPORT-----"

# Searching for the orthologue pairs in the dictionnaries
count = 0
best_hits.each_pair do |key, value|
    if best_hits_pep[value] == key
        File.open("report.txt", "a"){ |f|
            f.puts "The genes "+key+" and "+value+" are orthologues"
        }
        count += 1
    end
end

File.open("report.txt", "a"){ |f|
    f.puts "There are "+count.to_s+" orthologue pairs in our databases"
}

# Bibliography
# https://open.oregonstate.education/computationalbiology/chapter/command-line-blast/