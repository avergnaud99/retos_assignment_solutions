# README
## Bioinformatics Assignments

### Assignment1

The main script (mainScript.rb) is conceived to be in communication with the user of the project.
In fact, if you want to stop the compilation of the script you can do it at every step of the process by saying "no" to the line of command. A friendly question will ask you at every step if you want to continue the process or not.
I imagined this script in order for the user to have the time to see the outputs of every step one by one without being obliged to see all the outputs at once.

In order to test the detection of a wrong gene identifier, I had implemented in my code of the main script (line 24) a wrong gene identifier called "blablabla". If you want the process to continue normally, you have to modify the code by putting "list_gene[1][0]" instead of "blablabla" on line 24.

Thanks for having read the informations.

### Assignment 2

My approach of the problem was to first conceive a dictionnary of pair-interactions. In the mainScript, I have implemented a two-level depth of search for the interactions. In order to explain myself, the first one is about searching every gene that is interacting with every gene of our original list. Then we obtain our first version of the dictionnary containing our original genes as keys and their interactors as their values. The second level of depth is about searching every interactor of the interactors of our original genes. So, the first interactors of our original genes are put as keys in the dictionnary and their interactors as their values. 
If you want to go deeper, you may change the script of the file "mainScript.rb" at line 338 by changing the argument of the call of the function "compute_interactions()". This method is conceive to go as deep as you want.

Then, we have to build the networks. In order to do that I am constructing them level by level with a recursive function called "compute_network()". For example if we have :
a --> b,c,d
b --> x,y
c --> h,j
d --> e,f
e --> u
Then the algorithm will proceed step by step :
1 : [a,b], [a,c], [a,d]
2 : [a,b,x], [a,b,y], [a,c,h], [a,c,j], [a,d,e], [a,d,f]
3 : [a,b,x], [a,b,y], [a,c,h], [a,c,j], [a,d,e], [a,d,f], [a,d,e,u]
At every step, the network is getting bigger and bigger. In order to do that I analyze the last element of every unfinished network and I create a number of new networks that is equal to the number of interactors of the last element and I add the interactor for every one of them.

There two ways to finish the computation a network : by reaching the limit of size of it (I implemented a limit size of 10 but you are able to modify it in the code of the file "mainScript.rb" at line 300 by modifying the condition value of the variable $count minus 1 be careful !), or by having no new interactors at the next step. If the networks are finished, we create InteractionNetwork objects associated with them and we annotate every one of them with their KEGG and GO annotations by calling functions available in the InteractionNetwork Class.

Once we have all our networks, we analyze them one by one to see if they have zero, one or multiple gene(s) that was(were) in the original list of genes. And we report it in the final report with all of their KEGG's IDs and names, and GO's IDs and names. 

Every outputs and errors of the computation of the script are available in different files : 
- "errors_file.txt" for the errors of compilation (in fact it appends a lot to have issues of empty outputs in the web or unreachable rest api's)
- "dictionnaries_interactions.txt" for the pair-interactions by level of depth
- "networks.txt" for the networks
- "final_report.txt" for the final report

We also put a file called "ArabidopsisSubSubNetwork_GeneList.txt" that is containing only the first 10 genes of our original gene file. You can put the real file in the script by modifying the name of the entry file of the script of "mainScript.rb" at line 49. 

The problem we still have is that the time complexity of this script is too much high. I tried to improve it by small updates on the loops and the computations but it is still really long. For one gene at the origin, it could take a lot of minutes if one of the next genes have a lot of interactors.

### Assignment 3

The files created by the mainScript are already in the git repository of the assignment. In fact, it allows you to save time by not creating every EMBL file. 
The answers of the questions 4a and 5 are respectively in the files "CTTCTT.gff3" and "CTTCTT_chromosome.gff3".
The report of the genes without exons with the CTTCTT repeat is available by computing the mainScript (be careful because by computing the mainScript, the GFF3 files will be doubled). If you want to avoid any problem, please delete the GFF3 files and compute the mainScript, it will create them de novo. I just submitted them in order for you to check my answers at first.

My mainScript was conceived in order to be able to face every case of the EMBL files which makes the function "get_new_features" really large.

My answer to the last question aka the image of the GFF3 track beside the AT2G46340 gene on the ENSEMBL website is available in the files named "global_view.png" and "example.png".
The features are all located on the negative strand and we submitted a global view over the gene and also an example of a CTTCTT repeat. Note that the example is related to the exon : "AT2G46340.exon8".

### Assignment 4

First, as the BLAST gem was not working on my computer, I had to implement my code using terminal command lines. I have documented all my code in order for you to follow what I have done. This is the reason why I have to create a file for every gene in order to blast it with the command line of the terminal. 

The computation time of the code requires 10 hours. I have computed it and had resulted in 0 orthologue pairs (report in the file "report.txt"). 

There is a part of the dictionnary of the best hit of the S.pombe genes in the file "dict_pep.txt". It represents the best hit (lowest evalue) found by BLAST with the S.pombe genes as query and the Arabidopsis genes as target.

