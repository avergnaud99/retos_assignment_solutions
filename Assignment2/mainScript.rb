require "./InteractionNetwork_class.rb"
require 'rest-client'
require 'json'

# Accessing requests from the web
def fetch(url, headers = {accept: "*/*"}, user = "", pass="")
  response = RestClient::Request.execute({
    method: :get,
    url: url.to_s,
    user: user,
    password: pass,
    headers: {accept: 'application/json'}})
  return response
  
  rescue RestClient::ExceptionWithResponse => e
    $stderr.puts e.inspect
    response = false
    return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
  rescue RestClient::Exception => e
    $stderr.puts e.inspect
    response = false
    return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
  rescue Exception => e
    $stderr.puts e.inspect
    response = false
    return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
end 

# PSICQUIC REST APIs List sorted in a decreasing order of number of interactions in the database
# 10 databases : iREFIndex, BIOGRID, mentha, IMEx, IntAct, Reactome-FIs, Reactome, MINT, BAR, EBI-GOA-nonIntAct
$api_list = 
["https://irefindex.vib.be/webservices/current/search/interactor/", 
"http://tyersrest.tyerslab.com:8805/psicquic/webservices/current/search/interactor/",
"http://mentha.uniroma2.it:9090/psicquic/webservices/current/search/interactor/",
"http://www.ebi.ac.uk/Tools/webservices/psicquic/imex/webservices/current/search/interactor/", 
"http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices/current/search/interactor/",
"http://www.ebi.ac.uk/Tools/webservices/psicquic/reactome-fi/webservices/current/search/interactor/", 
"http://www.ebi.ac.uk/Tools/webservices/psicquic/reactome/webservices/current/search/interactor/",
"http://www.ebi.ac.uk/Tools/webservices/psicquic/mint/webservices/current/search/interactor/",
"http://bar.utoronto.ca:9090/psicquic/webservices/current/search/interactor/", 
"https://www.ebi.ac.uk/QuickGO/psicquic/webservices/current/search/interactor/"]

# Saving the genes from the original file in a dictionnary and a list
$interactions_agi = {}
$gene_origin = []
File.open("ArabidopsisSubSubSubNetwork_GeneList.txt", "r") { |file|
    file.each do |gene|
        $interactions_agi.store(gene.chomp.upcase, []) # putting every AGI locus with upcase letters to make the next computations easier
        $gene_origin.push(gene.chomp.upcase)
    end
}

# Computing the protein protein interactions
def compute_interactions(n)
    bool = true
    while n != 0 
        # First level of depth : searching every protein interaction for every key of the dictionnary
        if bool 
            $interactions_agi.keys.each do |gene|  
                api_num = 0
                res = ""
                $api_list.each do |api|
                    begin
                        res = fetch(api+gene+"?format=tab25").body
                    rescue Exception
                        File.open("errors_file.txt", "a"){ |f|
                            f.puts "In line 64 : Couldn't access the web API"
                        }   
                    end
                    break if res != ""
                    api_num += 1
                end
                
                # Treating the datas of the web to fit the algorithm
                list_interac = []
                pre_list_interac = res.split("\n")
                pre_list_interac.each do |inter|
                    list_interac.push(inter.split("\t")[0..3])
                end

                # Saving the interactors depending on their web source
                case api_num
                    # API n°1, 3, 4, 5, 6, 7, 8 and 9
                    when 0, 2..8
                        list_interac.each do |interac|
                            # Rescuing every call of the web if there is a problem
                            begin
                                agi_locus_1 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[0].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][0][0..8].upcase
                                agi_locus_2 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[1].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][0][0..8].upcase
                            rescue Exception
                                File.open("errors_file.txt", "a"){ |f|
                                    f.puts "In line 88 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                }
                            else
                                # Saving the neighbors avoiding the doubles
                                if agi_locus_1 != agi_locus_2
                                    if agi_locus_1 == gene
                                        if !($interactions_agi[agi_locus_1].include?(agi_locus_2))
                                            $interactions_agi[agi_locus_1].push(agi_locus_2)
                                        end
                                    else
                                        if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                            $interactions_agi[agi_locus_2].push(agi_locus_1)
                                        end
                                    end
                                end
                            end
                        end
                    # API n°2
                    when 1
                        list_interac.each do |interac|
                            begin
                                agi_locus_1 = interac[2].match(/entrez gene\/locuslink:A[Tt]\d[Gg]\d\d\d\d\d/).to_s[22..].upcase
                                agi_locus_2 = interac[3].match(/entrez gene\/locuslink:A[Tt]\d[Gg]\d\d\d\d\d/).to_s[22..].upcase
                            rescue Exception
                                File.open("errors_file.txt", "a"){ |f|
                                    f.puts "In line 113 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                }
                            else
                                # Saving the neighbors avoiding the doubles
                                if agi_locus_1 != agi_locus_2
                                    if agi_locus_1 == gene
                                        if !($interactions_agi[agi_locus_1].include?(agi_locus_2))
                                            $interactions_agi[agi_locus_1].push(agi_locus_2)
                                        end
                                    else
                                        if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                            $interactions_agi[agi_locus_2].push(agi_locus_1)
                                        end
                                    end
                                end
                            end
                        end
                    # API n°10
                    when 9
                        list_interac.each do |interac|
                            begin
                                agi_locus_1 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[0].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][-1].upcase
                                agi_locus_2 = interac[1][14..].upcase
                            rescue Exception
                                File.open("errors_file.txt", "a"){ |f|
                                    f.puts "In line 138 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                }
                            else
                                # Saving the neighbors avoiding the doubles
                                if agi_locus_1 != agi_locus_2
                                    if agi_locus_1 == gene
                                        if !($interactions_agi[agi_locus_1].include?(agi_locus_2))
                                            $interactions_agi[agi_locus_1].push(agi_locus_2)
                                        end
                                    else
                                        if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                            $interactions_agi[agi_locus_2].push(agi_locus_1)
                                        end
                                    end
                                end
                            end
                        end
                end
            end
            bool = false
            puts "---First level of interaction's depth complete---"
            puts "The first dictionnary of the interactions is available in the file 'dictionnaries_interactions.txt'."
            File.open("dictionnaries_interactions.txt", "a"){ |f|
                f.puts "---First level of interaction's depth---"
                $interactions_agi.to_a.each do |i|
                    f.puts i.to_s
                end
                f.puts "\n", "\n"
            }
            puts "\n"
        # Next levels of depth (number depending on the demand of the user)
        # Searching every protein interaction for every value of the dictionnary and saving every value as a knew key
        else
            $interactions_agi.values.each do |interactors|
                interactors.each do |gene|
                    if !($interactions_agi.has_key?(gene)) # Checking that the gene is not yet in the keys of the dictionnary
                        $interactions_agi.store(gene.upcase, []) 
                        api_num = 0
                        res = ""
                        $api_list.each do |api|
                            begin
                                res = fetch(api+gene+"?format=tab25").body
                            rescue Exception
                                File.open("errors_file.txt", "a"){ |f|
                                    f.puts "In line 183 : Couldn't access the web API"
                                }   
                            end
                            break if res != ""
                            api_num += 1
                        end
                        
                        # Treating the datas of the web to fit the algorithm
                        list_interac = []
                        pre_list_interac = res.split("\n")
                        pre_list_interac.each do |inter|
                            list_interac.push(inter.split("\t")[0..3])
                        end
                        
                        # Saving the interactors depending on their web source
                        case api_num
                            # API n°1, 3, 4, 5, 6, 7, 8 and 9
                            when 0, 2..8
                                list_interac.each do |interac|
                                    begin
                                        agi_locus_1 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[0].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][0][0..8].upcase
                                        agi_locus_2 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[1].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][0][0..8].upcase
                                    rescue Exception
                                        File.open("errors_file.txt", "a"){ |f|
                                            f.puts "In line 206 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                        }
                                    else
                                        # Saving the neighbors avoiding the doubles
                                        if agi_locus_1 != agi_locus_2
                                            if agi_locus_1 == gene
                                                if !($interactions_agi[agi_locus_1].include?(agi_locus_2))  # éviter les doublons
                                                    $interactions_agi[agi_locus_1].push(agi_locus_2)
                                                end
                                            else
                                                if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                                    $interactions_agi[agi_locus_2].push(agi_locus_1)
                                                end
                                            end
                                        end
                                    end
                                end
                            # API n°2
                            when 1
                                list_interac.each do |interac|
                                    begin
                                        agi_locus_1 = interac[2].match(/entrez gene\/locuslink:A[Tt]\d[Gg]\d\d\d\d\d/).to_s[22..].upcase
                                        agi_locus_2 = interac[3].match(/entrez gene\/locuslink:A[Tt]\d[Gg]\d\d\d\d\d/).to_s[22..].upcase
                                    rescue Exception
                                        File.open("errors_file.txt", "a"){ |f|
                                            f.puts "In line 231 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                        }
                                    else
                                        # Saving the neighbors avoiding the doubles
                                        if agi_locus_1 != agi_locus_2
                                            if agi_locus_1 == gene
                                                if !($interactions_agi[agi_locus_1].include?(agi_locus_2))
                                                    $interactions_agi[agi_locus_1].push(agi_locus_2)
                                                end
                                            else
                                                if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                                    $interactions_agi[agi_locus_2].push(agi_locus_1)
                                                end
                                            end
                                        end
                                    end
                                end
                            # API n°10
                            when 9
                                list_interac.each do |interac|
                                    begin
                                        agi_locus_1 = JSON.parse(fetch("http://togows.org/entry/ebi-uniprot/"+interac[0].match(/\:.*/).to_s[1..]+"/dr.json").body)[0]["Gramene"][0][-1].upcase
                                        agi_locus_2 = interac[1][14..].upcase
                                    rescue Exception
                                        File.open("errors_file.txt", "a"){ |f|
                                            f.puts "In line 256 : The AGI Locus Code of one of the interactors "+interac[0]+" or "+interac[1]+" is not correct"
                                        }
                                    else
                                        # Saving the neighbors avoiding the doubles
                                        if agi_locus_1 != agi_locus_2
                                            if agi_locus_1 == gene
                                                if !($interactions_agi[agi_locus_1].include?(agi_locus_2))
                                                    $interactions_agi[agi_locus_1].push(agi_locus_2)
                                                end
                                            else
                                                if !($interactions_agi[agi_locus_2].include?(agi_locus_1))
                                                    $interactions_agi[agi_locus_2].push(agi_locus_1)
                                                end
                                            end
                                        end
                                    end
                                end
                        end
                    end
                end
            end
            puts "---Next level of interaction's depth complete---"
            puts "The next dictionnary of the interactions is available in the file 'dictionnaries_interactions.txt'."
            File.open("dictionnaries_interactions.txt", "a"){ |f|
                f.puts "---Next level of interaction's depth---"
                $interactions_agi.to_a.each do |i|
                    f.puts i.to_s
                end
                f.puts "\n", "\n"
            }
            puts "\n"
        end
        n -= 1
    end
end

# Computing the networks by building every possible network in the interactions's dictionnary
def compute_network(networks)
    new_networks = []
    # Length limit for the networks : 10 
    if $count == 9
        networks.each do |net|
            File.open("networks.txt", "a"){ |f|
                f.puts net.to_s
            }
            newone = InteractionNetwork.new(:network => net)
            newone.annotate_kegg
            newone.annotate_go
        end
        return "---End of computation---"
    end
    networks.each do |n|
        skip = false
        if $count == 0
            $interactions_agi[n].each do |g|
                if n != g
                    new_networks.push([n,g])
                end
            end
        else
            if $interactions_agi[n.last] == [] || !($interactions_agi.has_key?(n.last))
                File.open("networks.txt", "a"){ |f|
                    f.puts n.to_s
                }
                newone = InteractionNetwork.new(:network => n)
                newone.annotate_kegg
                newone.annotate_go
                skip = true
            end
            next if skip
            $interactions_agi[n.last].each do |g|
                if !(n.include?(g))
                    new_networks.push(n + [g])
                end
            end
        end
    end
    $count += 1
    compute_network(new_networks)
end

# Calling the methods and computing the networks
compute_interactions(2)
puts "---Networks computation---"
puts "The networks of our ArabidopsisSubNetwork genes are available in the file 'networks.txt'."
$count = 0
puts compute_network($gene_origin)
puts "\n"

# Final report 
interacting_members = []
puts "---FINAL REPORT---"
puts "The final report is available in the file 'final_report.txt'."
boolean = false
# Searching for interactions in our original list of genes
$final = File.open("final_report.txt", "a")
InteractionNetwork.class_variable_get(:@@networks).each do |net|
    interacting_members.clear()
    net.network.each do |g|
        if $gene_origin.include?(g)
            interacting_members.push(g)
        end
    end
    func = lambda { |m| $final.print m.to_s+", "}
    if interacting_members.length() > 1
        interacting_members.each(&func)
        $final.puts "are interacting with one another."
        $final.puts "These are, respectively, their KEGG and GO functional annotations :"
        $final.print net.kegg.to_s+"\n"
        $final.print net.go.to_s+"\n"
        $final.puts "\n"
        boolean = true
    end
end
if !boolean 
    $final.puts "No genes are interacting with one another in our list."
end
puts "---End of the script---"

$final.close









