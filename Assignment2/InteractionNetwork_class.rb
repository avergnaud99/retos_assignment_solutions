require 'rest-client'
require 'json'

class InteractionNetwork 

    # Attributes
    attr_accessor :network
    attr_accessor :kegg
    attr_accessor :go

    # Class Variables
    @@networks = []
    @@keggs = {}
    @@gos = {}

    # Methods
    def initialize(params = {})
        @network = params.fetch(:network, [])
        @kegg = params.fetch(:kegg, {})
        @go = params.fetch(:go, {})

        @@networks.push(self)
    end

    # Accessing requests from the web
    def fetch(url, headers = {accept: "*/*"}, user = "", pass="")
        response = RestClient::Request.execute({
            method: :get,
            url: url.to_s,
            user: user,
            password: pass,
            headers: {accept: 'application/json'}})
        return response

        rescue RestClient::ExceptionWithResponse => e
            $stderr.puts e.inspect
            response = false
            return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
        rescue RestClient::Exception => e
            $stderr.puts e.inspect
            response = false
            return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
        rescue Exception => e
            $stderr.puts e.inspect
            response = false
            return response  # now we are returning 'False', and we will check that with an \"if\" statement in our main code
    end

    # Associating every gene of the network with its pathway's ID and names
    def annotate_kegg
        self.network.each do |g|
            # Improving the time complexity by not having to search keggs that have already been found
            if @@keggs.keys.include?(g)
                @@keggs[g].each do |k|
                    self.kegg.store(k[0], k[1])
                end
            else
                begin
                    list_keggs_id_pathway = fetch("http://rest.kegg.jp/link/pathway/ath:"+g).body.split("\n")
                rescue Exception
                    File.open("errors_file.txt", "a"){ |f|
                        f.puts "InteractionNetwork_class.rb line 59 : No pathways for : "+g
                    }
                end
                next if list_keggs_id_pathway == [] || list_keggs_id_pathway == nil
                @@keggs.store(g, [])
                list_keggs_id_pathway.each do |k|
                    couple = k.split("\t")
                    begin
                        name = JSON.parse(fetch("http://togows.org/entry/kegg-pathway/"+couple[1]+"/name.json").body)[0].split(" - ")[0]
                    rescue Exception
                        File.open("errors_file.txt", "a"){ |f|
                            f.puts "InteractionNetwork_class.rb line 70 : No name for the pathway : "+couple[1]
                        }
                    else
                        self.kegg.store(couple[1], name)
                    end
                    @@keggs[g].push([couple[1], name])
                end
            end
        end
    end

    # Associating every gene of the network with its GO's IDs and names
    def annotate_go
        self.network.each do |g|
            # Improving the time complexity by not having to search gos that have already been found
            if @@gos.keys.include?(g)
                if @@gos[g] == [] || @@gos[g] == nil 
                    self.go.store(g, "No GO")
                else
                    @@gos[g].each do |k|
                        self.go.store(k[0], k[1])
                    end
                end
            else
                begin
                    list_gos = JSON.parse(fetch("http://togows.dbcls.jp/entry/uniprot/"+g+"/dr.json").body)[0]["GO"]
                rescue Exception
                    File.open("errors_file.txt", "a"){ |f|
                        f.puts "InteractionNetwork_class.rb line 98 : No GO's for : "+g
                    }
                end
                @@gos.store(g, [])
                next if list_gos == [] || list_gos == nil
                list_gos.each do |p|
                    if p[1][0] == "P"
                        self.go.store(p[0], p[1])
                    end
                    @@gos[g].push([p[0], p[1]])
                end
            end
        end
    end
end
