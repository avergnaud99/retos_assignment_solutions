require "./SeedStock_class.rb"
require "./SeedStock_Database_class.rb"
require "./Gene_class.rb"

class HybridCross

    # Attributes
    attr_accessor :parent1
    attr_accessor :parent2
    attr_accessor :f2_wild
    attr_accessor :f2_P1
    attr_accessor :f2_P2
    attr_accessor :f2_P1P2

    # Class variables
    @@crosstable = []

    # Methods
    def initialize(params = {})
        @parent1 = params.fetch(:parent1, 'Unknown_Parent1')
        @parent2 = params.fetch(:parent2, 'Unknown_Parent2')
        @f2_wild = params.fetch(:f2_wild, 0)
        @f2_P1 = params.fetch(:f2_P1, 0)
        @f2_P2 = params.fetch(:f2_P2, 0)
        @f2_P1P2 = params.fetch(:f2_P1P2, 0)

        @@crosstable.push(self)
    end

    def chi_square_test(database)
        # We use the probability of 0.05 which significate that we are confident about our prediction with a 95% ratio. This is statistically enough for our experiment.
        # Also, we take 1 degree of liberty because we have 2 types of seed to compare with each other and the degree is equal to : the number of types - 1. 
        # So, we obtain the critical value by crossing the probability ad the degree of liberty in the Chi-Square Distribution Table.
        critical_value = 3.841

        # In order to compute the Chi-Square Test, we have to determine the expected values. 
        # The Punnett chess tells us that the repartition of the population should be : 9/3/3/1 (Wild/P1/P2/P1P2)
        expected_values = [9.0/16.0, 3.0/16.0, 3.0/16.0, 1.0/16.0]
        
        # Computing the Chi2
        chi2 = 0
        observed_values = [self.f2_wild.to_i, self.f2_P1.to_i, self.f2_P2.to_i, self.f2_P1P2.to_i]
        total_pop = observed_values.inject { |sum, v| sum + v }
        (0..expected_values.length-1).each do |i|
            chi2 += (observed_values[i] - expected_values[i]*total_pop)*(observed_values[i] - expected_values[i]*total_pop) / (expected_values[i]*total_pop)
        end

        # Collecting the datas of the genes
        stock1 = database.get_seed_stock(self.parent1)
        stock2 = database.get_seed_stock(self.parent2)
        gene1 = stock1.get_gene(stock1.gene_ID)
        gene2 = stock2.get_gene(stock2.gene_ID)

        # Testing the Chi2
        if chi2 <= critical_value
            puts "No difference between observed and expected : no link between the genes "+gene1.name+" and "+gene2.name+"."
        else 
            puts "Statistical difference between observed and expected : "+gene1.name+" is genetically linked to "+gene2.name+" with chisquare score "+chi2.to_s+"."
            gene1.linkage = gene1.name+' is linked to '+gene2.name
            gene2.linkage = gene2.name+' is linked to '+gene1.name
            puts "Please enter the gene information file in order to update it : "
            file = gets.chomp
            File.open(file, "w"){ |f|
                f << "Gene_ID\tGene_name\tmutant_phenotype\tLinkage\n"
                Gene.class_variable_get(:@@genelist).each do |g|
                    f << g.gene_ID+"\t"+g.name+"\t"+g.mutant_phenotype+"\t"+g.linkage+"\n"
                end
            }
        end
    end

end