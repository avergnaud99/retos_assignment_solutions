require "./SeedStock_class.rb"

class SeedStock_Database

    # Attributes
    attr_accessor :name
    attr_accessor :data_stock

    # Methods
    def initialize(params = {})
        @name = params.fetch(:name, 'database')
        @data_stock = params.fetch(:data_stock, [])
    end

    # Loading the file and putting every line of it into an new SeedStock Object.
    def load_from_file(seed_stock_data)
        list_stocks = [] 

        File.open(seed_stock_data, "r"){ |file|
            file.each do |line|
                list_stocks += [line.split("\t")]
            end
        }

        list_stocks[1..].each do |stock|
            @data_stock.push(SeedStock.new(:stock => stock[0], :gene_ID => stock[1], :date => stock[2], :storage => stock[3], :remainings => stock[4].chomp.to_i))
        end
    end

    # Updating a database from a new file by clearing and writing.
    def write_database(new_file)
        self.data_stock.clear()
        self.load_from_file(new_file)
    end

    # Getting a SeedStock Object from its ID.
    def get_seed_stock(stock_ID)
        SeedStock.class_variable_get(:@@seedstocklist).each do |stock|
            if stock.stock == stock_ID
                return stock
            end
        end
    end

end