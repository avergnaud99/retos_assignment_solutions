require "./Gene_class.rb"

class SeedStock 

    # Attributes
    attr_accessor :stock
    attr_accessor :gene_ID
    attr_accessor :date
    attr_accessor :storage
    attr_accessor :remainings

    # Class variables
    @@seedstocklist = []

    # Methods
    def initialize(params = {})
        @stock = params.fetch(:stock, 'Unknown_Stock')
        @gene_ID = params.fetch(:gene_ID, '000000000')
        @date = params.fetch(:date, '0/0/0000')
        @storage = params.fetch(:storage, "Unknown_Storage")
        @remainings = params.fetch(:remainings, 0)

        @@seedstocklist.push(self)
    end

    # Simulation of planting seven seeds of a specific seed stock and creation of a new stock file with the same format as the old one.
    def planting_seven_seeds(file)
        stock = @@seedstocklist.detect{|e| e.stock == self.stock}
        if stock.remainings >= 7
            stock.remainings -= 7
            if stock.remainings != 0
                puts "Remaining grams for the stock '"+stock.stock+"' : "+stock.remainings.to_s+"."
            else
                puts "No more seeds in the stock '"+stock.stock+"'."
            end
        else 
            print "We were only able to plant "+stock.remainings.to_s+" grams of this seed. "
            stock.remainings = 0
            puts "No more seeds in the stock '"+stock.stock+"'."
        end
        File.open(file, "w"){ |f|
            f << "Seed_Stock\tMutant_Gene_ID\tLast_Planted\tStorage\tGrams_Remaining\n"
            @@seedstocklist.each do |s|
                f << s.stock+"\t"+s.gene_ID+"\t"+s.date+"\t"+s.storage+"\t"+s.remainings.to_s+"\n"
            end
        }
    end

    # Getting a Gene Object from its ID.
    def get_gene(id)
        Gene.class_variable_get(:@@genelist).each do |gene|
            if gene.gene_ID == id
                return gene
            end
        end
    end

end