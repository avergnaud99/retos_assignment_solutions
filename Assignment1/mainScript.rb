require "./Gene_class.rb"
require "./SeedStock_Database_class.rb"
require "./SeedStock_class.rb"
require "./HybridCross_class.rb"

continue = "yes"
start = 0

while start == 0
    puts "Let's start ! First we will upload your genes into objects !", "\n"

    # UPLOADING THE GENE INFORMATIONS INTO A LIST
    list_gene = []
    #puts "Please enter your gene file : "
    #name_file = gets.chomp # for the user input I searched on https://ruby-doc.org/docs/ruby-doc-bundle/Tutorial/part_02/user_input.html 
    name_file = "gene_information.tsv"
    File.open(name_file, "r"){ |file|
        file.each do |line|
            list_gene += [line.chomp.split("\t")]
        end
    }

    # CREATION OF THE GENES AND TRYING TO IMPLANT A FALSE ID
    gene1 = Gene.new(:gene_ID => "blablabla", :name => list_gene[1][1], :mutant_phenotype => list_gene[1][2])
    gene2 = Gene.new(:gene_ID => list_gene[2][0], :name => list_gene[2][1], :mutant_phenotype => list_gene[2][2])
    gene3 = Gene.new(:gene_ID => list_gene[3][0], :name => list_gene[3][1], :mutant_phenotype => list_gene[3][2])
    gene4 = Gene.new(:gene_ID => list_gene[4][0], :name => list_gene[4][1], :mutant_phenotype => list_gene[4][2])
    gene5 = Gene.new(:gene_ID => list_gene[5][0], :name => list_gene[5][1], :mutant_phenotype => list_gene[5][2])

    if $error = "yes" 
        start = 1
        puts "Report : \nSorry, we were not able to upload your gene datas into a Gene Object.\nYour Gene Identifier should match the pattern '/A[Tt]\d[Gg]\d\d\d\d\d/'\n"
    end
    break if start == 1

    puts "Now we will create your SeedStock Objects, do you want to continue ? (yes or no)", "\n"
    continue = gets.chomp

    if continue == "yes"
        # CREATION OF THE SEED STOCKS
        database = SeedStock_Database.new()
        database.load_from_file("seed_stock_data.tsv")
        puts "These are the stocks and their datas of your database named '"+database.name+"' : "+database.data_stock.to_s, "\n"
    else
        start = 1
    end
    break if start == 1

    puts "Then, we will test the 'get_seed_stock' function, do you want to continue ? (yes or no)", "\n"
    continue = gets.chomp

    if continue == "yes"
        # TESTING THE FUNCTION "get_seed_stock" FROM THE DATABASE CLASS
        puts "Which stock do you want to see ?"
        request = gets.chomp
        begin
            stockrequest = database.get_seed_stock(request)
            puts "This is the stock "+stockrequest.stock+", it contains the gene "+stockrequest.gene_ID+" and there are still "+stockrequest.remainings.to_s+" grams remaining.", "\n"
        rescue 
            puts "Your stock is not in our database, please enter an other stock : (last chance sorry...)"
            request = gets.chomp
            stockrequest = database.get_seed_stock(request)
            puts "This is the stock "+stockrequest.stock+", it contains the gene "+stockrequest.gene_ID+" and there are still "+stockrequest.remainings.to_s+" grams remaining.", "\n"
        end
    else
        start = 1
    end
    break if start == 1

    puts "Okay, now let's plant some seeds. Do you want to continue ? (yes or no)", "\n"
    continue = gets.chomp

    if continue == "yes"
        # PLANTING SEEDS
        puts "What is the name of the file where you want to put your new informations ? (it should end with '.tsv')"
        new_file = gets.chomp

        puts "\n"
        database.data_stock.each do |ss|
            ss.planting_seven_seeds(new_file)
        end
        database.write_database(new_file)

        puts "\n", "These are your new seedstocks : "+database.data_stock.to_s, "\n"
    else
        start = 1
    end
    break if start == 1

    puts "Now let's upload your crosses into HybridCross Objects. Do you want to continue ? (yes or no)", "\n"
    continue = gets.chomp

    if continue == "yes"
        # UPLOADING THE CROSS INFORMATIONS INTO A LIST
        list_crosses = [] 
        #puts "Please enter your cross file : "
        #name_cross = gets.chomp   
        name_cross = "cross_data.tsv" 
        File.open(name_cross, "r"){ |file|
            file.each do |line|
                list_crosses += [line.split("\t")]
            end
        }

        # CREATING NEW HybridCross OBJETCS
        list_crosses[1..].each do |cross|
            HybridCross.new(:parent1 => cross[0], :parent2=> cross[1], :f2_wild => cross[2].to_i, :f2_P1 => cross[3].to_i, :f2_P2 => cross[4].to_i, :f2_P1P2 => cross[5].chomp.to_i)
        end

        puts "\n"
    else
        start = 1
    end
    break if start == 1

    puts "Finally we will do a ChiSquare Test on these crosses. Do you want to continue ? (yes or no)", "\n"
    continue = gets.chomp

    if continue == "yes"
        # COMPUTING A CHI-SQUARE TEST
        HybridCross.class_variable_get(:@@crosstable).each do |hybrid|
            hybrid.chi_square_test(database)
        end
    else
        start = 1
    end
    break if start == 1

    puts "\n"

    # FINAL REPORT
    puts "Final Report : ", "\n"
    Gene.class_variable_get(:@@genelist).each do |g|
        if g.linkage != "No linkage"
            puts g.linkage
        end
    end

    start = 1
end












