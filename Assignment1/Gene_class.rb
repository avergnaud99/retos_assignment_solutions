class Gene

    # Attributes
    attr_accessor :gene_ID
    attr_accessor :name
    attr_accessor :mutant_phenotype
    attr_accessor :linkage

    # Class variables
    @@genelist = []

    # Methods
    def initialize(params = {})
        # We use rescue in order to check that the Gene Identifier is fitting the right regular expression in order to be able to create a Gene Object.
        $error = "no"
        begin
            gene_ID = params.fetch(:gene_ID, '000000000')
            if (/A[Tt]\d[Gg]\d\d\d\d\d/.match(gene_ID)) == nil
                raise StandardError.new "This is an exception"
            end
        rescue Exception
            $error = "yes"
        else    
            @gene_ID = params.fetch(:gene_ID, '000000000')
            @name = params.fetch(:name, 'gene')
            @mutant_phenotype = params.fetch(:mutant_phenotype, '...')
            @linkage = params.fetch(:linkage, 'No linkage')
        end

        @@genelist.push(self)
    end

end

# Documentation for raising an error :
# https://rollbar.com/guides/ruby/how-to-raise-exceptions-in-ruby/ 
# https://stackify.com/rescue-exceptions-ruby/ 

