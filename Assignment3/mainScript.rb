require 'net/http'
require 'bio'

### Methods

# Searching for the EMBL files and saving them in order to obtain the biosequence object.
def get_bioseq(gene)
    bool = false
    # Searching and saving the file only if it is not yet on the repository
    if !File.file?('./EMBL_files/'+gene+'.embl')
        address = URI('http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=ensemblgenomesgene&format=embl&id='+gene) 
        response = Net::HTTP.get_response(address)
        record = response.body

        File.open('./EMBL_files/'+gene+'.embl', 'w') do |f|
            f.puts record
        end
    end

    # Computing the biosequence object
    if Bio::FlatFile.autodetect_file('./EMBL_files/'+gene+'.embl') != nil
        datas = Bio::FlatFile.auto('./EMBL_files/'+gene+'.embl')
        entry =  datas.next_entry
        bioseq = entry.to_biosequence
        return [bioseq, gene]
    else
        return nil
    end
end

# Creating new features by analysing every exon looking for the cttctt pattern and saving the coordinates 
def get_new_features(bioseq, gene, new_features = [])
    bioseq.features.each do |f|
        bool = false
        # Only if the feature is an exon of the gene
        if f.feature == 'exon'
            id = f.qualifiers[0].value
            # First case : if the exon is on the positive strand
            if /complement/.match(f.position).to_s == ""
                # Testing if the exon sequence is on the sequence of the gene or if the coordinates correspond to the sequence
                if id.split(/[=\.]/)[1] == gene || /\d+\D{2}\d+/.match(f.position).to_s.split("..")[1].to_i <= bioseq.seq.length
                    positions = /\d+\D{2}\d+/.match(f.position).to_s.split("..")
                    sub_seq = bioseq.seq[positions[0].to_i-1..positions[1].to_i-1]
                    m_data = []
                    pos_i = 0
                    while pos_i <= sub_seq.length-6
                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                        else
                            pos_i = sub_seq.length
                        end
                    end
                # Second case : if the exon is on an other gene in the report of our gene
                else
                    bioseq_complement = get_bioseq(id.split(/[=\.]/)[1])
                    if bioseq_complement != nil
                        bioseq_complement = bioseq_complement[0]
                        bioseq_complement.features.each do |bc|
                            if bc.feature == 'exon' && bc.qualifiers[0].value == id
                                positions = /\d+\D{2}\d+/.match(bc.position).to_s.split("..")
                                # If the exon is on the positive strand of this new gene
                                if /complement/.match(bc.position).to_s == ""
                                    sub_seq = bioseq_complement.seq[positions[0].to_i-1..positions[1].to_i-1]
                                    m_data = []
                                    pos_i = 0
                                    while pos_i <= sub_seq.length-6
                                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                                        else
                                            pos_i = sub_seq.length
                                        end
                                    end
                                # If the exon is on the complementary strand of this new gene
                                else
                                    bool = true
                                    sub_seq = bioseq_complement.seq[positions[0].to_i-1..positions[1].to_i-1].reverse_complement
                                    m_data = []
                                    pos_i = 0
                                    while pos_i <= sub_seq.length-6
                                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                                        else
                                            pos_i = sub_seq.length
                                        end
                                    end
                                end
                            else
                                m_data = ""
                            end
                        end
                    else
                        m_data = ""
                    end
                end
                # Creating the features according to the case it is from
                if m_data.to_s != ""
                    if bool
                        (0..m_data.size-1).each do |i|
                            id = f.qualifiers[0].value
                            new_features.push(Bio::Feature.new('cttctt', (positions[1].to_i-m_data[i][1]).to_s+'..'+(positions[1].to_i-m_data[i][0]-1).to_s, 
                            [Bio::Feature::Qualifier.new('strand', '-'), Bio::Feature::Qualifier.new('exon', id), Bio::Feature::Qualifier.new('complement', id.split(/[=\.]/)[1]), Bio::Feature::Qualifier.new('pos+', (m_data[i][0]+positions[0].to_i).to_s+'..'+(m_data[i][1]+positions[0].to_i-1).to_s)]))
                        end
                    else
                        (0..m_data.size-1).each do |i|
                            id = f.qualifiers[0].value
                            new_features.push(Bio::Feature.new('cttctt', (m_data[i][0]+positions[0].to_i-1).to_s+'..'+(m_data[i][1]+positions[0].to_i-2).to_s, 
                            [Bio::Feature::Qualifier.new('strand', '+'), Bio::Feature::Qualifier.new('exon', id)]))
                        end
                    end
                end
            # Third case : the exon is on the complementary strand of the gene
            else
                # If the exon sequence is on the sequence of the gene or if the coordinates correspond to the sequence
                if id.split(/[=\.]/)[1] == gene || /\d+\D{2}\d+/.match(f.position).to_s.split("..")[1].to_i <= bioseq.seq.length
                    positions = /\d+\D{2}\d+/.match(f.position).to_s.split("..")
                    sub_seq = bioseq.seq[positions[0].to_i-1..positions[1].to_i-1].reverse_complement
                    m_data = []
                    pos_i = 0
                    while pos_i <= sub_seq.length-6
                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                        else
                            pos_i = sub_seq.length
                        end
                    end
                # Fourth case : the exon is on an other gene related to our gene
                else
                    bioseq_complement = get_bioseq(id.split(/[=\.]/)[1])
                    if bioseq_complement != nil
                        bioseq_complement = bioseq_complement[0]
                        bioseq_complement.features.each do |bc|
                            if bc.feature == 'exon' && bc.qualifiers[0].value == id
                                positions = /\d+\D{2}\d+/.match(bc.position).to_s.split("..")
                                # If the exon is on the positive strand of this new gene
                                if /complement/.match(bc.position).to_s == ""
                                    bool = true
                                    sub_seq = bioseq_complement.seq[positions[0].to_i-1..positions[1].to_i-1]
                                    m_data = []
                                    pos_i = 0
                                    while pos_i <= sub_seq.length-6
                                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                                        else
                                            pos_i = sub_seq.length
                                        end
                                    end
                                # If the exon is on the complementary strand of this new gene
                                else
                                    sub_seq = bioseq_complement.seq[positions[0].to_i-1..positions[1].to_i-1].reverse_complement
                                    m_data = []
                                    pos_i = 0
                                    while pos_i <= sub_seq.length-6
                                        if /(cttctt)/.match(sub_seq, pos_i) != nil
                                            m_data.push([/(cttctt)/.match(sub_seq, pos_i).offset(1)[0], /(cttctt)/.match(sub_seq, pos_i).offset(1)[1]])
                                            pos_i = /(cttctt)/.match(sub_seq, pos_i).offset(1)[0]+1
                                        else
                                            pos_i = sub_seq.length
                                        end
                                    end
                                end
                            else
                                m_data = ""
                            end
                        end
                    else
                        m_data = ""
                    end
                end
                # Creating the features according to the case it is from
                if m_data.to_s != "" 
                    if bool
                        (0..m_data.size-1).each do |i|
                            new_features.push(Bio::Feature.new('cttctt', (m_data[i][0]+positions[0].to_i-1).to_s+'..'+(m_data[i][1]+positions[0].to_i-2).to_s, 
                            [Bio::Feature::Qualifier.new('strand', '+'), Bio::Feature::Qualifier.new('exon', id)]))
                        end
                    else
                        (0..m_data.size-1).each do |i|
                            new_features.push(Bio::Feature.new('cttctt', (positions[1].to_i-m_data[i][1]).to_s+'..'+(positions[1].to_i-m_data[i][0]-1).to_s, 
                            [Bio::Feature::Qualifier.new('strand', '-'), Bio::Feature::Qualifier.new('exon', id), Bio::Feature::Qualifier.new('complement', gene), Bio::Feature::Qualifier.new('pos+', (m_data[i][0]+positions[0].to_i).to_s+'..'+(m_data[i][1]+positions[0].to_i-1).to_s)]))
                        end
                    end
                end
            end
        end
    end

    return new_features
end

# Creating the GFF3 files
def get_gff3(file1, file2, bioseq)
    bioseq.features.each do |ft|
        if ft.feature == 'cttctt'
            positions = ft.position.split("..")
            # GFF3 file with coordinates relative to the gene
            File.open(file1, "a"){ |f|
                if ft.qualifiers.length >= 3
                    f.puts bioseq.entry_id+"\tEnsembl\t"+ft.feature+"\t"+positions[0]+"\t"+positions[1]+"\t.\t"+ft.qualifiers[0].value+"\t.\tGene="+ft.qualifiers[1].value.split(/[=\.]/)[1]+";Name="+ft.qualifiers[1].value+";Complement_of="+ft.qualifiers[2].value
                else
                    f.puts bioseq.entry_id+"\tEnsembl\t"+ft.feature+"\t"+positions[0]+"\t"+positions[1]+"\t.\t"+ft.qualifiers[0].value+"\t.\tGene="+ft.qualifiers[1].value.split(/[=\.]/)[1]+";Name="+ft.qualifiers[1].value
                end
            }
            # GFF3 file with the coordinates relative to the chromosome
            File.open(file2, "a"){ |f|
                if ft.qualifiers.length >= 3
                    chr_coordinates = get_full_coordinates(ft.qualifiers[2].value)
                    if chr_coordinates != nil
                        f.puts bioseq.entry_id+"\tEnsembl\t"+ft.feature+"\t"+(chr_coordinates[0].to_i+positions[0].to_i).to_s+"\t"+(chr_coordinates[0].to_i+positions[1].to_i).to_s+"\t.\t"+ft.qualifiers[0].value+"\t.\tGene="+ft.qualifiers[1].value.split(/[=\.]/)[1]+";Name="+ft.qualifiers[1].value+";Complement_of="+ft.qualifiers[2].value
                    end
                else
                    chr_coordinates = get_full_coordinates(ft.qualifiers[1].value.split(/[=\.]/)[1])
                    if chr_coordinates != nil
                        f.puts bioseq.entry_id+"\tEnsembl\t"+ft.feature+"\t"+(chr_coordinates[0].to_i+positions[0].to_i).to_s+"\t"+(chr_coordinates[0].to_i+positions[1].to_i).to_s+"\t.\t"+ft.qualifiers[0].value+"\t.\tGene="+ft.qualifiers[1].value.split(/[=\.]/)[1]+";Name="+ft.qualifiers[1].value
                    end
                end
            }
        end
    end
end

# Getting the coordinates of the cttctt patterns on the chromosome base
def get_full_coordinates(gene)
    if File.file?('./EMBL_files/'+gene+'.embl')
        datas = Bio::FlatFile.auto('./EMBL_files/'+gene+'.embl')
        entry =  datas.next_entry
        chr_coordinates = [entry.sv.split(":")[3].to_i, entry.sv.split(":")[4].to_i]
        return chr_coordinates
    else
        return nil
    end
end

### Main

# Creating the list of genes
list_genes = []
File.open("ArabidopsisSubNetwork_GeneList.txt", "r"){ |f|
    f.each do |l|
        list_genes.push(l.chomp.upcase)
    end
}

# Creating every Sequence Object 
list_bioseqs = []
list_genes.each do |g|
    list_bioseqs.push(get_bioseq(g))
end

# Searching for every CTTCTT pattern, adding them all to the features of the Sequence object and putting it into a GFF3 file
File.open("CTTCTT.gff3", "a"){ |f|
    f.puts "##gff-version 3"
    f.puts "\n"
}

File.open("CTTCTT_chromosome.gff3", "a"){ |f|
    f.puts "##gff-version 3"
    f.puts "\n"
}

list_bioseqs.each do |b|
    new_features = get_new_features(b[0], b[1])
    new_features.each do |n|
        b[0].features.push(n)
    end
    get_gff3('CTTCTT.gff3', 'CTTCTT_chromosome.gff3', b[0])
end

# Searching for gene without CTTCTT pattern
File.open("CTTCTT.gff3", "r"){ |f|
    header = ["##gff-version 3\n", "\n"]
    gene_pattern = []
    f.each do |l|
        if !(header.include?(l))
            if l.split("\t")[6] == "-"
                name = l.split("\t")[8].split(/[=\;]/)[6].chomp
            else
                name = l.split("\t")[8].split(/[=\;]/)[1]
            end
            if list_genes.include?(name) && !(gene_pattern.include?(name))
                gene_pattern.push(name)
            end
        end
    end

    puts "---FINAL REPORT---"
    count = 0
    list_genes.each do |g|
        if !(gene_pattern.include?(g))
            puts "The gene "+g+" do not have exons with the CTTCTT repeat"
            count += 1
        end   
    end
    puts "--> There are "+count.to_s+" genes without exons that have the CTTCTT repeat"
}











